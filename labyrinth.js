function haxxert(condition){
	if(!condition){
		console.trace("haxxert failed!")
		process.exit() // not meaningful to continue!
	}
}

function getCellRecurse(matrix, index_vector, depth){
	if (depth == index_vector.length - 1) {
		return matrix[index_vector[depth]]
	}
	else if (depth >= index_vector.length ){
		haxxert(0)
	}
	else {
		return getCellRecurse(matrix[index_vector[depth]], index_vector, depth + 1)
	}
}

function getCell(matrix, index_vector){
	return getCellRecurse(matrix, index_vector, 0)
}

var Cell = function(dimensions, id){
	this.id = id
	this.walls = [] 
	for(var i=0;i<dimensions;i++){ // walls only exist in positive direction
		this.walls.push(1)
	}
	this.visited = 0
}

function buildGridRecurse(sizes, dimension, id){
	var array = []

	haxxert(sizes.length >= dimension)

	if(dimension == sizes.length-1){
		for(var i=0; i<sizes[dimension]; i++){
			array.push(new Cell(sizes.length, id[0]++))
		}
	}
	else{
		for(var i=0; i<sizes[dimension]; i++){
			array.push(buildGridRecurse(sizes, dimension+1, id))
		}
	}

	return array
}

var getCellsRecurse = function(grid, coord_vector, dimensions){
	// can be used as static
	var cells = {}
	var dimension = coord_vector.length

	haxxert(dimension < dimensions)

	if(dimension == dimensions-1){
		for(var i=0; i<grid.length; i++){
			var c = coord_vector.slice()
			c.push(i)
			cells[c] = grid[i]		
		}
	}
	else{
		for(var i=0; i<grid.length; i++){
			var c = coord_vector.slice()
			c.push(i)
			var new_cells = getCellsRecurse(grid[i], c, dimensions)
			for (var attrname in new_cells) { 
				cells[attrname] = new_cells[attrname]; 
			}
		}
	}

	return cells
}

var Grid = function(sizes){
	this.size = []
	for(var i=0;i<sizes.length;i++){
		this.size.push(sizes[i])
	}

	var id=[0]
	this.cells = buildGridRecurse(this.size, 0, id)

	this.getCells = function(){
		return getCellsRecurse(this.cells, [], this.size.length)
	}
}

var Labyrinth = function(grid, poi){ // refactor start, stop to be a list of POI (point of interest)
	this.grid = grid
	
	this.start = poi[0]
	this.stop = poi[1]
	this.path = [] //store the indices of the solution

	var dimensions = grid.size.length

	// a bunch of tests
	haxxert(dimensions == start.length)
	for(var d=0; d<dimensions; d++){
		haxxert(start[d] >= 0 && start[d] <= grid.size[d])
	}

	// construct the labyrinth
	var junctions = [start.slice()] // keep a stack of all cells traversed so far that had possible junctions

	while(junctions.length){
		var pos = junctions.pop()
		
		var current_cell = getCell(this.grid.cells, pos)
		current_cell.visited += 1 // for debugging

		haxxert(current_cell.walls.length == dimensions)

		var atStop = true
		for(var d=0; d<dimensions; d++){
			atStop = atStop && pos[d] == stop[d]
		}

		if(!atStop){

			var triedWalls = []
			for(var i=0; i<2*dimensions; i++){
				triedWalls.push(0)
			}

			var triedAllWalls = false
			while(!triedAllWalls){ // todo, optimize		

				var tryWall = Math.floor(Math.random()*2*dimensions)
				var tryPos = pos.slice()
				triedWalls[tryWall] = 1

				var tryDim = tryWall%dimensions
				var tryDir = Math.pow(-1, tryWall >= dimensions)

				tryPos[tryDim] += tryDir

				var targetInGrid = true
				for(var d=0; d<dimensions; d++){
					targetInGrid = targetInGrid && tryPos[d] >= 0 && tryPos[d] < grid.size[d]
				}

				triedAllWalls = true
				for(var i=0; i<triedWalls.length; i++){
					triedAllWalls = triedAllWalls && triedWalls[i]
				}

				if( targetInGrid ){
					var target_cell = getCell(this.grid.cells, tryPos)
					if(!target_cell.visited){
						if(tryDir > 0){
							current_cell.walls[tryDim] = 0
						}
						else{
							target_cell.walls[tryDim] = 0
						}

						junctions.push(pos) // push back on junctions (it's more optimal to not push back on if triedAllWalls. However, it's better to do if we want to keep the path without skipping corners ;)
						junctions.push(tryPos)
						break;
					}
				}
				else{
					if(tryDir > 0){
						current_cell.walls[tryDim] = -2 // for debugging
					}
				}
			}
		}
		else{
			// at stop, store away the path to here
			for(var i=0;i<junctions.length;i++){
				this.path.push(junctions[i])
			}
			this.path.push(pos)
		}
	}
}